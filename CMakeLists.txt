# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2016)
# Author: Arnaud Blanchard

cmake_minimum_required(VERSION 2.6)

get_filename_component(PROJECT_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME) 
project(${PROJECT_NAME})

find_package(blc_program)
find_package(blc_channel)

add_definitions(${BL_DEFINITIONS})
include_directories(${BL_INCLUDE_DIRS})
add_executable(${PROJECT_NAME} main.cpp)
target_link_libraries(${PROJECT_NAME} ${BL_LIBRARIES})
 